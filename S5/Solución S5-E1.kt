val PEDIDO_MINIMO_DONAS = 20 // Pedido mínimo de donas

// Arreglo con la cantidad de ingredientes necesarios para cada dona
val cantidadIngrediente = arrayOf(
    20.0, // Harina
    1.0,  // Levadura
    7.5,  // Azucar
    3.75, // Leche
    1.5,  // Mantequilla
    0.15, // Huevo
    0.05, // Sal
    12.0  // Aceite
)

var nombrePrograma: String? = null // Almacena y muestra el nombre del programa

fun main() {

    nombrePrograma = "\n## Ingredientes y costo de pedidos de donas ##" // Asignación del nombre del programa
    println(nombrePrograma!!) // Impresión del nombre del programa

    val totalDonas: Int

    try {
        // Lectura de los datos
        print("\nTotal de donas a elaborar: ")
        totalDonas = readLine()!!.toInt()
    } catch (error: Exception) {
        // Excepción que se produce si el valor ingresado no se puede convertir a entero
        println("Se debe ingresar un número entero, intenta nuevamente")
        return main() // Inicia nuevamente
    }

    // Se válida que sea un pedido correcto en cuanto a la cantidad de donas a elaborar
    if (totalDonas < PEDIDO_MINIMO_DONAS) {
        println("El pedido debe ser de mínimo $PEDIDO_MINIMO_DONAS donas") // Muestra el mensaje de error
        return main() // Inicia nuevamente
    }

    // Calcula el total a cobrar por el pedido, dependiendo de la cantidad y el precio que aplique
    val costoPedido = if (totalDonas < 100) {
        totalDonas * 6.0 // Costo por dona para menos de 100
    } else {
        totalDonas * 5.55 // Costo por dona, para 100 o más donas
    }

    // Imprime los resultados, realizando los calculos y redondeando el resultado
    println(
        """
        ***********************************
        Ingredientes para $totalDonas donas

        Harina       | ${Math.round(cantidadIngrediente[0] * totalDonas)} gramos
        Levadura     | ${Math.round(cantidadIngrediente[1] * totalDonas)} gramos
        Azucar       | ${Math.round(cantidadIngrediente[2] * totalDonas)} gramos
        Leche        | ${Math.round(cantidadIngrediente[3] * totalDonas)} mililitros
        Mantequilla  | ${Math.round(cantidadIngrediente[4] * totalDonas)} gramos
        Huevo        | ${Math.round(cantidadIngrediente[5] * totalDonas)} huevos
        Sal          | ${Math.round(cantidadIngrediente[6] * totalDonas)} gramos
        Aceite       | ${Math.round(cantidadIngrediente[7] * totalDonas)} mililitros
        ***********************************
        Total a cobrar: $${Math.round(costoPedido)}
        ***********************************
    """.trimIndent()
    )

    main()

}