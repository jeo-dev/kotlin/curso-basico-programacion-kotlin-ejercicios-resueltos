/**
 * Enum con las acciones permitidas en el programa al manipular
 * la cuenta bancaria
 */
enum class Accion {
    DEPOSITAR {
        override fun toString(): String {
            return "Depósito de dinero"
        }
    },
    RETIRAR {
        override fun toString(): String {
            return "Retiro de dinero"
        }
    },
}