import java.math.BigDecimal

// IMPORTANTE:  Si es necesario, agregar el import de la clase Cuenta y del enum Accion

/*
 * Instanciación del objeto con los datos iniciales, debe ser declarado de manera global para que se
 * mantengan los datos cuando se llame nuevamente a main() para realizar más operaciones
 */
val objetoCuenta = Cuenta("01-8547-9", "Alberto Palma", BigDecimal("5000.00"))

fun main() {
    // Muestra el menú e indica que se ingrese la accion deseada
    print(
        """
        Menú
        1 - Depositar
        2 - Retirar

        Ingresa la acción a realizar: 
    """.trimIndent()
    )

    // Realiza la lectura del valor y su asignación, si es una acción incorrecta finaliza la ejecución del programa
    val accion = when (readLine()) {
        "1" -> Accion.DEPOSITAR
        "2" -> Accion.RETIRAR
        else -> return println("Saliendo del programa...")
    }

    // Realiza la lectura del monto y hace las validaciones necesarias para asegurar que sea un dato válido
    print("Ingresa el monto: ")
    // Se utiliza BigDecimal ya que tiene una precision exacta para trabajar con números
    val monto = readLine()?.toBigDecimalOrNull()
    if (monto == null || monto <= BigDecimal("0")) {
        println("Monto incorrecto\n")
        return main()
    }

    // Realiza la acción indicada, y asigna el resultado a la variable resultado
    val resultado = if (accion == Accion.DEPOSITAR) {
        objetoCuenta.depositar(monto)
    } else {
        objetoCuenta.retirar(monto)
    }

    // Si el resultado es false es que no se pudo realizar la acción
    if (!resultado) {
        println("No se pudo realizar la acción solicitada\n")
        return main()
    }

    // Al llegar a este punto, el proceso fue correcto, ahora se muestra el resultado
    println(
        """
        -------------- Recibo --------------

        ${objetoCuenta.obtenerDatosCuenta()}

        Acción: $accion
        Monto: $$monto
        Saldo anterior: $${objetoCuenta.saldoAnterior}
        Nuevo Saldo: $${objetoCuenta.saldo}

         -----------------------------------
    """.trimIndent()
    )

    main()
}