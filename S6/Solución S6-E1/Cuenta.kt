import java.math.BigDecimal

/**
 * Clase cuenta para almacenar datos bancarios de usuarios
 * @param clabe clabe de la cuenta
 * @param titular nombre del titular de la cuenta
 * @param saldo saldo inicial al crear la cuenta
 */
class Cuenta constructor(val clabe: String, val titular: String, var saldo: BigDecimal) {

    // Auxiliar para almacenar el saldo anterior en operaciones
    var saldoAnterior: BigDecimal = saldo

    /**
     * Permite agregar dinero a la cuenta
     * @param valor Monto a depositar
     * @return Boolean indica si la operación fue exitosa
     * */
    fun depositar(valor: BigDecimal): Boolean {
        // Se valida que el valor a depositar sea positivo
        return if (valor > BigDecimal("0")) {
            saldoAnterior = saldo
            saldo += valor
            true
        } else {
            false
        }
    }

    /**
     * Permite retirar dinero del saldo actual, realizando validaciones
     * para evitar que el saldo de la cuenta quede negativo
     * @return Boolean indica si la operación fue exitosa
     * */
    fun retirar(valor: BigDecimal): Boolean {

        val valorFinal = valor.abs() // Obtiene el valor absoluto

        return if (valorFinal > saldo) {
            // Si el valor a retirar es mayor que el saldo regresa false y ya no continua la ejecucion dem método
            println("El monto a retirar ($$valorFinal) no puede ser mayor que el saldo ($${saldo})")
            false
        } else {
            // Si es correcto, se asigna el saldo anterior y se quita el valor al saldo
            saldoAnterior = saldo
            saldo -= valorFinal
            true
        }
    }

    /**
     * @return String Datos de la cuenta, clabe y titular en formato de texto
     * */
    fun obtenerDatosCuenta(): String {
        return "Clabe: $clabe - Titular: $titular"
    }

}