import java.math.BigDecimal

// imports necesarios si se crean las clases en otros archivos
import clases.Electrodomestico
import clases.Lavadora
import clases.Microondas
import clases.Refrigerador
import enums.ColorElectrodomestico

fun main() {

    println("\n## Fabricación de Electrodomésticos ##\n")

    // Se crea un objeto de cada clase y se inicializa con algunos datos de prueba
    val lavadora1 = Lavadora("SD5S84DRTR", ColorElectrodomestico.BLANCO, 25.00, BigDecimal("9500.99"), 18.00)
    val microondas1 = Microondas("AOS87DPSOR", ColorElectrodomestico.NEGRO, 8.50, BigDecimal("2100.00"), 1350.00)
    val refrigerador1 = Refrigerador("SPDO8756SR", ColorElectrodomestico.GRIS, 47.0, BigDecimal("7899.00"), 10.00, 00.00, -15.00)

    // Se crea una lista y se agregan todos los electrodomésticos
    val listaElectrodomesticos = arrayOf(lavadora1, microondas1, refrigerador1)

    // Se itera la lista y se imprime en la consola
    listaElectrodomesticos.forEach {
        println(it)
    }

    // Se imprime el contador estatico de la clase Electrodomestico, que nos indica cuantas instancias de la clase se han creado
    println("\nTotal de productos fabricados: ${Electrodomestico.contadorFabricados}")

}