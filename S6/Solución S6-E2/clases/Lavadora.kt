package clases

import enums.ColorElectrodomestico
import enums.TipoElectrodomestico
import java.math.BigDecimal

/**
 * Clase para las lavadoras que hereda de Electrodomestico
 * Requiere todos los parámetros de electrodomésticos más los siguientes:
 * @param capacidad
 * */
class Lavadora(
    numeroSerie: String,
    color: ColorElectrodomestico,
    peso: Double,
    precio: BigDecimal,
    var capacidad: Double
) : Electrodomestico(numeroSerie, TipoElectrodomestico.LAVADORA, color, peso, precio) {

    override fun toString(): String {
        return super.toString() + " - Capacidad: $capacidad kg"
    }

}