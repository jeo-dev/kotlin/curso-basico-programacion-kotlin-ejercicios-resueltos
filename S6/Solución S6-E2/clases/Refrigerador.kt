package clases

import enums.ColorElectrodomestico
import enums.TipoElectrodomestico
import java.math.BigDecimal

/**
 * Clase para los refrigeradores que hereda de Electrodomestico
 * Requiere todos los parámetros de electrodomésticos más los siguientes:
 * @param capacidad
 * @param temperaturaMinimaConservador
 * @param temperaturaMinimaCongelador
 * */
class Refrigerador(
    numeroSerie: String,
    color: ColorElectrodomestico,
    peso: Double,
    precio: BigDecimal,
    var capacidad: Double,
    var temperaturaMinimaConservador: Double,
    var temperaturaMinimaCongelador: Double
) : Electrodomestico(numeroSerie, TipoElectrodomestico.REFRIGERADOR, color, peso, precio) {

    override fun toString(): String {
        return super.toString() + " - Capacidad: $capacidad ft³ - Temperatura mínima: [$temperaturaMinimaConservador °C conservador][$temperaturaMinimaCongelador °C congelador]"
    }

}