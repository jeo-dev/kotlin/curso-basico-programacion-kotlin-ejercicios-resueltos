package clases

import enums.ColorElectrodomestico
import enums.TipoElectrodomestico
import java.math.BigDecimal

/**
 * Clase base para los electrodomésticos
 * @param numeroSerie
 * @param tipo
 * @param color
 * @param peso
 * @param precio
 * */
abstract class Electrodomestico(
    var numeroSerie: String,
    var tipo: TipoElectrodomestico,
    var color: ColorElectrodomestico,
    var peso: Double,
    var precio: BigDecimal
) {

    // Cada vez que se crea una instancia de la clase, se aumenta el contador
    init {
        contadorFabricados++
    }

    // Se sobrescribe el método toString para mostrar los datos
    override fun toString(): String {
        return "Número de Serie: $numeroSerie - Tipo: $tipo - Color: $color - Peso: $peso kg - Precio: $$precio"
    }

    companion object {
        // Se declara una variable estatica que pertenece a la clase para contabilizar cuantas veces se han creado instancias de la clase
        var contadorFabricados = 0
    }
}