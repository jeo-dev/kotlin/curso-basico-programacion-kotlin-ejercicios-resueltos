package clases

import enums.ColorElectrodomestico
import enums.TipoElectrodomestico
import java.math.BigDecimal

/**
 * Clase para los microondas que hereda de Electrodomestico
 * Requiere todos los parámetros de electrodomésticos más los siguientes:
 * @param potencia
 * */
class Microondas(
    numeroSerie: String,
    color: ColorElectrodomestico,
    peso: Double,
    precio: BigDecimal,
    var potencia: Double
) : Electrodomestico(numeroSerie, TipoElectrodomestico.MICROONDAS, color, peso, precio) {

    override fun toString(): String {
        return super.toString() + " - Potencia: $potencia watts"
    }

}