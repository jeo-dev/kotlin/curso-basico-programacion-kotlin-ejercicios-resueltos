package enums

/**
 * Enum para los colores de los electrodomésticos
 * */
enum class ColorElectrodomestico {
    BLANCO {
        override fun toString(): String {
            return "Blanco"
        }
    },
    NEGRO {
        override fun toString(): String {
            return "Negro"
        }
    },
    GRIS {
        override fun toString(): String {
            return "Gris"
        }
    }
}