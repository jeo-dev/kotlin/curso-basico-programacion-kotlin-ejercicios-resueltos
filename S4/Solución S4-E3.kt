fun main() {

    // Mensaje
    println("Ingresa los números uno por uno, para ver el resultado ingresa 0")

    // Declaración de variables
    var contador = 0 // Para identificar cuantos números se han ingresado
    var suma = 0.0 // Cada vez que se ingresa un numero se almacena el nuevo valor de la suma
    var numeroEntrada: Double // Número que ingresa el usuario

    do {
        numeroEntrada = readLine()!!.toDouble() // Se asigna el nuevo número
        if (numeroEntrada != 0.0) contador++ // Se incrementa el contador solo si la entrada es diferente de cero
        suma += numeroEntrada // Se suma el valor actual más la entrada
    } while (numeroEntrada != 0.0) // Se repite el ciclo hasta que se ingrese un cero

    // Imprime los resultados
    println("Suma: $suma") // La suma directamente
    println("Promedio: ${suma / contador}\n") // Calcula el promedio con la división y muestra el resultado

    main() // Llama nuevamente a main para poder hacer más operaciones

}