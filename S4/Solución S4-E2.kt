﻿// Lista de alumnos
val alumnos = arrayOf(
        "José",
        "Alberto",
        "Laura",
        "Noel",
        "Erika",
        "Daniel"
)

// Lista de calificaciones, correspondiente a los alumnos
val calificaciones = arrayOf(
        31.0, // José
        94.0, // Alberto
        98.5, // Laura
        75.0, // Noel
        46.5, // Erika
        75.0  // Daniel
)

val PROMEDIO_MINIMO_APROBATORIO = 60.0 // Promedio mínimo requerido para pasar aprobar

fun main() {
    calcularMostrarPromedio()
    mostrarCalificacionMasAlta()
    mostrarCalificacionMasBaja()
    mostrarAlumnosReprobados()
}

/**
 * Calcula e imprime el promedio
 * */
fun calcularMostrarPromedio() {
    var promedio = 0.0

    // Ciclo para iterar el arreglo
    for (calificacion in calificaciones) {
        promedio += calificacion // Sumatoria de las calificaciones
    }

    promedio = promedio / calificaciones.size // Division de la suma entre el total de elementos
    println("Promedio de calificación: $promedio") // Se imprime el resultado
}

/**
 * Determina e imprime la calificación más alta
 * */
fun mostrarCalificacionMasAlta() {
    var resultado = 0.0 // Auxiliar para almacenar la calificación más alta

    // Ciclo para iterar el arreglo
    for (calificacion in calificaciones) {
        // Si la calificación es mayor que la mejor calificación hasta el momento
        if (calificacion > resultado) {
            resultado = calificacion // Se asigna la nueva calificación más alta
        }
    }

    println("Calificación más alta: $resultado") // Se imprime el resultado
}

/**
 * Determina e imprime la calificación más baja
 * */
fun mostrarCalificacionMasBaja() {

    //Se ordena el arreglo y se obtiene el valor de la posición cero, que debe ser la calificación más baja
    val resultado = calificaciones.sortedArray()[0]

    println("Calificación más baja: $resultado") // Se imprime el resultado
}

/**
 * Determina e imprime los nombres y calificaciones de los alumnos reprobados
 * */
fun mostrarAlumnosReprobados() {

    println("\nReprobados:")

    // Se realiza una iteración obteniendo las posiciones desde 0 hasta el tamaño del arreglo -1 con until
    for (i in 0 until calificaciones.size) {
        // Si la calificación es menor que ma mínima aprobatoria
        if (calificaciones[i] < PROMEDIO_MINIMO_APROBATORIO) {
            println("${alumnos[i]}, ${calificaciones[i]}") // Imprime el nombre y la calificación de los reprobados
        }
    }
}