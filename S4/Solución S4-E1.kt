val COSTO_POR_KILOMETRO = 2.25 // Variable global con el valor del costo por kilómetro
val PORCENTAJE_DESCUENTO = 45 // Variable global con el valor del descuento en porcentaje

fun main() {

    // Entrada de datos
    print("Kilómetros de trayecto: ")
    val kilometros = readLine()!!

    // Si no se ingresa al valor, se llama a main para que inicie nuevamente el programa
    if (kilometros.isBlank()) {
        println("Se deben ingresar los kilómetros")
        return main()
    }

    print("Aplicar descuento: ")
    val aplicarDescuento = readLine()!! // Se obtiene como string

    val tarifaGeneral = COSTO_POR_KILOMETRO * kilometros.toDouble() // Se calcula la tarifa general

    when (aplicarDescuento) {
        // En caso de que sea un si o un 1, aplica el descuento
        "si", "1" -> {
            val totalConDescuento = (tarifaGeneral / 100) * (100 - PORCENTAJE_DESCUENTO)
            println("Total a cobrar: $${Math.round(totalConDescuento)}\n")
        }
        // En cualquier otro caso muestra la tarifa general
        else -> {
            println("Total a cobrar: $${Math.round(tarifaGeneral)}\n")
        }
    }

    main() // Llama nuevamente a main para poder hacer más operaciones

}