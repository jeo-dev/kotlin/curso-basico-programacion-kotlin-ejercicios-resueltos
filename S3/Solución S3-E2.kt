﻿import java.text.DecimalFormat

fun main() {

    // Mensaje opcional de instrucciones
    println("Ingresa los siguientes datos")

    // Indicaciones, entrada y asignación de datos

    print("Salario mensual bruto: ")
    val salarioMensualBruto = readLine()!!.toDouble()

    print("Impuesto mensual (porcentaje): ")
    val impuestoMensualPorcentaje = readLine()!!.toDouble()

    // Cálculo de los datos al mes
    val impuestoMensualValor = (salarioMensualBruto / 100) * impuestoMensualPorcentaje
    val salarioMensualNeto = salarioMensualBruto - impuestoMensualValor

    // Cálculo de los datos al año
    val impuestoAnualValor = impuestoMensualValor * 12
    val salarioAnualNeto = salarioMensualNeto * 12

    val formato = "#.##" // Formato para mostrar los datos de salida, con un máximo de dos posiciones decimales

    // Impresión de los resultados
    println(
        """

        Salario mensual neto: $${DecimalFormat(formato).format(salarioMensualNeto)}
        Impuestos a pagar por mes: $${DecimalFormat(formato).format(impuestoMensualValor)}

        Salario anual neto: $${DecimalFormat(formato).format(salarioAnualNeto)}
        Impuestos a pagar por año: $${DecimalFormat(formato).format(impuestoAnualValor)}

    """.trimIndent()
    )

}