﻿import java.text.DecimalFormat

val PI = 3.14159265359 // Variable global con el valor aproximado de pi

fun main() {

    // Mensaje opcional
    println("Calcular área y perímetro de un círculo")

    // Entrada de datos
    print("Ingresa el radio en centímetros: ")
    val radio = readLine()!!.toDouble()

    val area = calcularAreaCirculo(radio) // El resultado de la función se asigna a la variable
    val circunferencia = calcularCircunferenciaCirculo(radio)  // El resultado de la función se asigna a la variable

    val formato = "#.##" // Formato para mostrar los datos de salida, con un máximo de dos posiciones decimales

    // Muestra los resultados
    println(
        """

        *** Centímetros ***
        Área               | ${DecimalFormat(formato).format(area)}
        Circunferencia     | ${DecimalFormat(formato).format(circunferencia)}

        *** Pulgadas ***
        Área               | ${DecimalFormat(formato).format(convertirCm2aIn2(area))}
        Circunferencia     | ${DecimalFormat(formato).format(convertirCentimetrosAPulgadas(circunferencia))}


        """.trimIndent()
    )

    main() // Llama a la función main nuevamente para poder calcular el área y perímetro de otro círculo
}

/**
 * Calcula el área de un círculo
 * @param radio el radio del círculo
 * @return el área del círculo
 * */
fun calcularAreaCirculo(radio: Double): Double {
    return PI * (radio * radio)
}

/**
 * En base a al [radio] de un círculo calcula y regresa el valor de la circunferencia
 * */
fun calcularCircunferenciaCirculo(radio: Double): Double {
    return 2 * PI * radio
}

/**
 * Convierte [centimetros] a pulgadas
 * */
fun convertirCentimetrosAPulgadas(centimetros: Double): Double {
    return (1 / 2.54) * centimetros
}

/**
 * Convierte [centimetrosCuadrados] a pulgadas cuadradas
 * */
fun convertirCm2aIn2(centimetrosCuadrados: Double): Double {
    return 0.1550 * centimetrosCuadrados
}