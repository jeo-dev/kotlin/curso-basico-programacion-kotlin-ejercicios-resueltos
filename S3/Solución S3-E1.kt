﻿fun main() {

    // Mensaje opcional de instrucciones
    println("Ingresa los siguientes datos")

    // Indicaciones, entrada y asignación de datos

    print("Calle: ")
    val calle = readLine()

    print("Ciudad: ")
    val ciudad = readLine()

    print("Estado: ")
    val estado = readLine()

    print("País: ")
    val pais = readLine()

    print("Código postal: ")
    val codigoPostal = readLine()

    // Se muestra el resultado concatenando la información
    println("Dirección completa: $calle, $ciudad, $estado, $pais, $codigoPostal\n")
    
}